import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    df['Title'] = df['Name'].str.extract(r'(Mr\.|Mrs\.|Miss\.)')
    missing_age = df[df['Age'].isna()].groupby('Title').size()
    median = df.groupby('Title')['Age'].median()
    Mr = ('Mr.',missing_age['Mr.'],round(median['Mr.']))
    Mrs = ('Mrs.',missing_age['Mrs.'],round(median['Mrs.']))
    Miss = ('Miss.', missing_age['Miss.'], round(median['Miss.']))


    return [Mr,Mrs,Miss]
